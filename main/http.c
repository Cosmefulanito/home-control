/*
 * http.c
 *
 *  Created on: 9 mar. 2021
 *      Author: sguti
 */

#include "main.h"

uint32_t old_ipv4;
const char http_html_hdr[]= "HTTP/1.1 200 OK\r\nContent-type: text/html\r\n\r\n";
char get_rsp[][20]= {"luz_1/encender", "luz_1/apagar", "luz_2/encender", "luz_2/apagar", };
char page_path[][50]= {"/spiffs/WEBpage.htm", "/spiffs/WEBpage1.htm", "/spiffs/WEBpage2.htm", "/spiffs/WEBpage3.htm", "/spiffs/WEBpage4.htm"};

/*variables externas*/
extern char strftime_buf[64];
extern QueueHandle_t time_sem_handler;

// Reverses a string 'str' of length 'len'
void reverse(char* str, int len)
{
    int i = 0, j = len - 1, temp;
    while (i < j) {
        temp = str[i];
        str[i] = str[j];
        str[j] = temp;
        i++;
        j--;
    }
}

// Converts a given integer x to string str[].
// d is the number of digits required in the output.
// If d is more than the number of digits in x,
// then 0s are added at the beginning.
int intToStr(int x, char str[], int d)
{
    int i = 0;
    while (x) {
        str[i++] = (x % 10) + '0';
        x = x / 10;
    }

    // If number of digits required is more, then
    // add 0s at the beginning
    while (i < d)
        str[i++] = '0';

    reverse(str, i);
    str[i] = '\0';
    return i;
}

// Converts a floating-point/double number to a string.
void ftoa(float n, char* res, int afterpoint)
{
    // Extract integer part
    int ipart = (int)n;

    // Extract floating part
    float fpart = n - (float)ipart;

    // convert integer part to string
    int i = intToStr(ipart, res, 0);

    // check for display option after point
    if (afterpoint != 0) {
        res[i] = '.'; // add dot

        // Get the value of fraction part upto given no.
        // of points after dot. The third parameter
        // is needed to handle cases like 233.007
        fpart = fpart * pow(10, afterpoint);

        intToStr((int)fpart, res + i + 1, afterpoint);
    }
}

void http_server_log(struct netconn *conn)
{
	/* Envio address */
    struct ip_addr addr;
    uint16_t sockPORT=0;
    char *ipv4, sockTYPE[50];
    /*Obtengo la ip local*/
    netconn_getaddr(conn, &addr , &sockPORT, 0);
    ipv4 = ipaddr_ntoa((const ip_addr_t*)&addr.u_addr.ip4.addr);
    if ( old_ipv4 != addr.u_addr.ip4.addr ) {
    	old_ipv4 = addr.u_addr.ip4.addr;
        printf("\n********************************");
        printf("\nConnected to ipv4: %s", ipv4);
        unsigned int sockID = conn->type;
        switch( sockID ){
        	case NETCONN_TCP:              strcpy(sockTYPE , "NETCONN_TCP"); break;
            case NETCONN_TCP_IPV6:         strcpy(sockTYPE , "NETCONN_TCP_IPV6"); break;
            case NETCONN_UDP:              strcpy(sockTYPE , "NETCONN_UDP"); break;
            case NETCONN_UDPLITE:          strcpy(sockTYPE , "NETCONN_UDPLITE"); break;
            case NETCONN_UDPNOCHKSUM:      strcpy(sockTYPE , "NETCONN_UDPNOCHKSUM"); break;
            case NETCONN_UDP_IPV6:         strcpy(sockTYPE , "NETCONN_UDP_IPV6"); break;
            case NETCONN_UDPLITE_IPV6:     strcpy(sockTYPE , "NETCONN_UDPLITE_IPV6"); break;
            case NETCONN_UDPNOCHKSUM_IPV6: strcpy(sockTYPE , "NETCONN_UDPNOCHKSUM_IPV6"); break;
            case NETCONN_RAW:              strcpy(sockTYPE , "NETCONN_RAW "); break;
        }
        printf("\nsocket type: %s", sockTYPE);
        printf("\nport: %d", sockPORT);
        printf("\n********************************\n");
    }
}

int8_t getIDentification(char * x)
{
        uint8_t i, k, lenght, flag=0;

        for(k=0; k<GET_n; k++, flag=0){
                lenght = strlen(get_rsp[k]);
                for(i=0; i<lenght; i++)
                        if( x[i] != get_rsp[k][i] ){
                                flag = 1;
                                i = lenght;
                        }
                if(flag==0)
                        return k+1;
        }
        return 0;
}

void http_server_netconn(struct netconn *conn)
{
  struct netbuf *inbuf;//network buffer -> contiene los datos y el address
  char *buf, _cnvbuff[5];
  u16_t buflen, recv_timeout = 500;
  err_t err;
  extern char *_mptr; /*definida en spiffs.c*/
  extern sensor_t dht_var; /*definida en dht11.c*/

  /* Envia la info del cliente por terminal */
  http_server_log(conn);

  netconn_set_recvtimeout(conn, recv_timeout);//seteo un timeout
  /* Leo los datos del puerto, bloquea en caso de que no haya dato que leer (timeout=500mseg) */
  err = netconn_recv(conn, &inbuf);

  if (err == ERR_OK) {

    netbuf_data(inbuf, (void**)&buf, &buflen);//obtengo un puntero al bufer netbuf 'buf y su longitud 'buflen'

    /* Recibi HTTP GET? solo chequeo los primeros 5 caracteres */
    if( buflen >=  5  &&
        buf[0] == 'G' &&
        buf[1] == 'E' &&
        buf[2] == 'T' &&
        buf[3] == ' ' &&
        buf[4] == '/' ) {

        /* Seteo los pines de las luces como salida */
        gpio_pad_select_gpio(GPIO_LUZ_1);
        gpio_pad_select_gpio(GPIO_LUZ_2);
        gpio_set_direction(GPIO_LUZ_1, GPIO_MODE_OUTPUT);
        gpio_set_direction(GPIO_LUZ_2, GPIO_MODE_OUTPUT);

        /* Obtengo la direccion ipv4 del cliente */
        struct ip_addr addr;
        uint16_t port;
        char *ipv4;
        netconn_getaddr(conn, &addr , &port, 0);
        ipv4 = ipaddr_ntoa((const ip_addr_t*)&addr.u_addr.ip4.addr);

        /* Identifico el GET */
        int8_t id = getIDentification(&buf[5]);
        switch(id){
            case 1:
                printf("\n%s --- Luz #1 Encendida", ipv4);
                gpio_set_level(GPIO_LUZ_1, LUZ_ON);
                break;
            case 2:
                printf("\n%s --- Luz #1 Apagada", ipv4);
                gpio_set_level(GPIO_LUZ_1, LUZ_OFF);

                break;
            case 3:
                printf("\n%s --- Luz #2 Encendida", ipv4);
                gpio_set_level(GPIO_LUZ_2, LUZ_ON);

                break;
            case 4:
                printf("\n%s --- Luz #2 Apagada\n", ipv4);
                gpio_set_level(GPIO_LUZ_2, LUZ_OFF);
                break;
        }
        /* Leo el .html desde el spiffs y envio el contenido de la pagina */
        uint32_t file_lenght = spiffs_read(page_path[id]);
        /* Envio el header HTML */
        http_send_txt(conn, http_html_hdr);
        /* Envio la pagina HTML */
        netconn_write(conn, _mptr, file_lenght, NETCONN_NOCOPY);
        free(_mptr);//libero el bloque de memoria

        /* Presentacion sensores temperatura */
        ftoa(dht_var.temp, _cnvbuff, 2);
        http_send_txt(conn, "<hr><h3>SENSOR DHT11: </h3>");
        http_send_txt(conn, "<table border=\"1\"><tr><td>Temperatura</td><td >Humedad</td></tr>");
        http_send_txt(conn, "<td><h1><font color=\"#cc0000\">");
        netconn_write(conn, _cnvbuff, 5, NETCONN_NOCOPY);//con 5bytes mando dos cifras despues de la coma
        http_send_txt(conn, " *C");
        http_send_txt(conn, "</font></h1></td>");
        http_send_txt(conn, "<td><h1><font color=\"#cc0000\">");
        ftoa(dht_var.humd, _cnvbuff, 2);
        netconn_write(conn, _cnvbuff, 5, NETCONN_NOCOPY);
        http_send_txt(conn, " %");
        http_send_txt(conn, "</font></h1></td></hr>");

        /* Presentacion fecha y hora */
        xSemaphoreGive(time_sem_handler);
        http_send_txt(conn, "<br>Hora y fecha: ");
        http_send_txt(conn, strftime_buf);
        }
  }

  /* Cierro la conexion */
  netconn_close(conn);
  /* Libero el buffer */
  netbuf_delete(inbuf);
}

void app_http_server(void *pvParameters)
{
  struct netconn *conn, *newconn;
  err_t err;
  conn = netconn_new(NETCONN_TCP);//Creo una nueva estructura para la conexion TCP
  netconn_bind(conn, NULL, 80);   //Seteo el PORT=80. Le solicito a la red una direccion ipv4

  netconn_listen(conn);//listen state -> esperando por nuevas conexiones...

  while(1){

	 err = netconn_accept(conn, &newconn);//Espero por nuevas conexiones... bloquea el proceso hasta que se conecten los clientes

	 if (err == ERR_OK){
       http_server_netconn(newconn);
       netconn_delete(newconn);}
  	 }
   	 netconn_close(conn);
   	 netconn_delete(conn);
}
