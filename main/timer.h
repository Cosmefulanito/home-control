/*
 * timer.h
 *
 *  Created on: 9 mar. 2021
 *      Author: sguti
 */

#ifndef MAIN_TIMER_H_
#define MAIN_TIMER_H_

#define TIMER_DIVIDER  80  						      /* divisor del timer*/
#define TIMER_SCALE    (TIMER_BASE_CLK/TIMER_DIVIDER) /* base de tiempo -> (BASE_CLK)/80 = 80MHZ/80= 1MHZ*/
#define AUTO_RELOAD    1
#define WITHOUT_RELOAD 0

#define BLOCK_TO_ALARM_EVENT 	 0
#define NOT_BLOCK_TO_ALARM_EVENT 1

volatile uint8_t ISR_TimerFLAG;

void timer_Init(int timer_idx, bool auto_reload);
void timer_Count_To(int timer_idx, double timer_interval_usec, bool mode);
void timer_Reset(int timer_idx);

#endif /* MAIN_TIMER_H_ */
