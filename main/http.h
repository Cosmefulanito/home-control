/*
 * http.h
 *
 *  Created on: 9 mar. 2021
 *      Author: sguti
 */

#ifndef MAIN_HTTP_H_
#define MAIN_HTTP_H_

#define GPIO_LUZ_1 0  //pin luz 1
#define GPIO_LUZ_2 2  //pin luz 2

#define GET_n   4
#define LUZ_ON  0
#define LUZ_OFF 1

/*envia los datos por el socket TCP*/
#define http_send_txt(conn, txt) netconn_write(conn, txt, sizeof(txt)-1, NETCONN_NOCOPY)

void reverse(char* str, int len);
int intToStr(int x, char str[], int d);
void ftoa(float n, char* res, int afterpoint);
void http_server_log(struct netconn *conn);
int8_t getIDentification(char * x);
void http_server_netconn(struct netconn *conn);
void app_http_server(void *pvParameters);

#endif /* MAIN_HTTP_H_ */
