/*
 * spiffs.h
 *
 *  Created on: 9 mar. 2021
 *      Author: sguti
 */

#ifndef MAIN_SPIFFS_H_
#define MAIN_SPIFFS_H_

uint32_t spiffs_read( char * file);
int spiffs_init(const char *spiffs_path);

#endif /* MAIN_SPIFFS_H_ */
