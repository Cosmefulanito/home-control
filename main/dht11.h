/*
 * dht11.h
 *
 *  Created on: 9 mar. 2021
 *      Author: sguti
 */
#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#define DHT_SENSOR 23 //pin DHT11

#define uSeg_LowStartConvertion  20000
#define uSeg_HighStartConvertion 30
#define uSeg_ACKrsp   		     80
#define uSeg_DataComming         50
#define uSeg_LogicHigh        	 70
#define uSeg_LogicLow            26
#define uSeg_TimeOUT	         20000

#define _CHECKSUMERROR 4
#define _DATAERROR	   3
#define _ACKERROR      2
#define _TIMEOUT       1
#define _SUCCESS       0

typedef struct {
	float temp;
	float humd;
}sensor_t;

/* Prototipos */
void dht11_init(uint8_t dht11_pin);
void dht11_init_xfer( uint8_t dht11_pin );
uint8_t dht11_read( sensor_t *dht_x , uint8_t dht_pin );
void app_dht11(void *arg);

#ifdef __cplusplus
}
#endif
