/*
 * spiffs.c
 *
 *  Created on: 9 mar. 2021
 *      Author: sguti
 */

#include "main.h"

const char *TAG = "HOMEControl";
char *_mptr = NULL;

uint32_t spiffs_read( char * file )
{
	const char *CURRENT_TAG = "spiffs_read";
    ESP_LOGI(CURRENT_TAG, "Opening file: %s", file);

    // abro el archivo para lectura
    FILE* f = fopen(file, "r");
    if (f == NULL) {
        ESP_LOGE(CURRENT_TAG, "Failed to open file");
        return 0;
    }

    /*Determino el tamaño del archivo*/
    unsigned int len=0;
    fseek(f, 0, SEEK_END);//posiciono la ventana en el final del archivo
    len = ftell(f);//mido los bytes
    ESP_LOGI(CURRENT_TAG, "File lenght: %d bytes", len);
    rewind(f);

    /*Realizo la lectura*/
    _mptr = (char *) malloc(len);//gestiono el bloque de memoria
    memset(_mptr, 0, len);
    fread(_mptr , 1, len, f);
    fclose(f);
    // Imprimo el contenido del archivo
    ESP_LOGI(CURRENT_TAG, "Reading done correctly! \n");

    return len;
}

int spiffs_init(const char *spiffs_path)
{
	const char *CURRENT_TAG = "spiffs_init";
    ESP_LOGI(CURRENT_TAG, "Inicializando SPIFFS . . .");
    ESP_LOGI(CURRENT_TAG, "PATH SPIFFS: %s", spiffs_path);

    esp_vfs_spiffs_conf_t conf = {
      .base_path = spiffs_path,
      .partition_label = NULL,
      .max_files = 5,
      .format_if_mount_failed = false
    };

    // inicializo y monto el filesystem
    esp_err_t ret = esp_vfs_spiffs_register(&conf);
    if (ret != ESP_OK) {
        if (ret == ESP_FAIL) {
            ESP_LOGE(CURRENT_TAG, "Failed to mount or format filesystem");
        } else if (ret == ESP_ERR_NOT_FOUND) {
            ESP_LOGE(CURRENT_TAG, "Failed to find SPIFFS partition");
        } else {
            ESP_LOGE(CURRENT_TAG, "Failed to initialize SPIFFS (%s)", esp_err_to_name(ret));
        }
        return -1;
    }

    size_t total = 0, used = 0;
    ret = esp_spiffs_info(NULL, &total, &used);
    if (ret != ESP_OK) {
        ESP_LOGE(CURRENT_TAG, "Failed to get SPIFFS partition information (%s)", esp_err_to_name(ret));
        return -1;
    } else {
        ESP_LOGI(TAG, "Partition size: total: %d, used: %d", total, used);
    }
    return 1;
}
