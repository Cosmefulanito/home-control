/*
 * dht11.c
 *
 *  Created on: 9 mar. 2021
 *      Author: sguti
 */
#include "main.h"

sensor_t dht_var;
const char dht_ret_log[][50] = {"SUCCESS", "TIMEOUT", "ACK ERROR", "DATA ERROR", "CHECKSUM ERROR"};

void dht11_init(uint8_t dht11_pin)
{
	/* Configuro el registro IOMUX para el pin del DTH11 */
	gpio_pad_select_gpio(dht11_pin);

	gpio_config_t io_conf;

	/* Inicializo el GPIO */
	io_conf.intr_type = GPIO_INTR_DISABLE;//desactivo interrupciones
	io_conf.mode = GPIO_MODE_INPUT; 	  //seteo el pin como entrada
	io_conf.pin_bit_mask = 1<<dht11_pin;	      //mask en el pin de datos del DHT11
	io_conf.pull_down_en = 0;			  //desactivo pull-down
	io_conf.pull_up_en = 1;				  //activo pull-up
	gpio_config(&io_conf);			      //transfiero la estructura
}

void dht11_init_xfer( uint8_t dht11_pin )
{
	/* ----------------
	 * Start convertion
	 * ---------------- */
	gpio_set_direction(dht11_pin, GPIO_MODE_INPUT);
	timer_Count_To(TIMER_1, 1000, BLOCK_TO_ALARM_EVENT);
	/* Pongo un '0' en el bus -> start signal */
	gpio_set_direction(dht11_pin, GPIO_MODE_OUTPUT);//seteo el pin como salida
	gpio_set_level(dht11_pin, 0);
	/* Sostengo el '0' durante >18mseg */
	timer_Count_To(TIMER_1, uSeg_LowStartConvertion, BLOCK_TO_ALARM_EVENT);
	/* Pongo el bus en alta impedancia (30useg por default) y espero respuesta del sensor ... */
	gpio_set_direction(dht11_pin, GPIO_MODE_INPUT);//seteo el pin como entrada
	timer_Count_To(TIMER_1, uSeg_HighStartConvertion, BLOCK_TO_ALARM_EVENT);
}

uint8_t dht11_read(sensor_t * dht_x , uint8_t dht_pin)
{
	uint64_t timerCurrentValue=0;
	uint8_t pos=7, nbyte=0, rbuff[]={0, 0, 0, 0, 0}, nbits;

	/* Espero a que el sensor ponga el bus en '0' */
	timer_Count_To(TIMER_1, uSeg_TimeOUT, NOT_BLOCK_TO_ALARM_EVENT);//seteo un timeout en caso de que el sensor no responda
	while( gpio_get_level(dht_pin) && !ISR_TimerFLAG );
	/* Reseteo el timer */
	timer_set_counter_value(TIMER_GROUP_0, TIMER_1, 0x00000000ULL);
	/*Espero por el '0' de 80useg*/
	while( !gpio_get_level(dht_pin) && !ISR_TimerFLAG );
	timer_get_counter_value(TIMER_GROUP_0, TIMER_1, &timerCurrentValue);
	if( timerCurrentValue > uSeg_ACKrsp+10 )
		return _ACKERROR;
	else if( ISR_TimerFLAG )
			return _TIMEOUT;
	timer_set_counter_value(TIMER_GROUP_0, TIMER_1, 0x00000000ULL);
	/* Espero por el '1' de 80useg */
	while( gpio_get_level(dht_pin) && !ISR_TimerFLAG );
	timer_get_counter_value(TIMER_GROUP_0, TIMER_1, &timerCurrentValue);
	if( timerCurrentValue > uSeg_ACKrsp+10 )
		return _ACKERROR;
	else if( ISR_TimerFLAG )
			return _TIMEOUT;
	/*Recibo la trama de datos*/
	for( nbits=0; nbits<40; nbits++ ){
		timer_set_counter_value(TIMER_GROUP_0, TIMER_1, 0x00000000ULL);
		/* Espero por el '0' de comienzo del bit */
		while( !gpio_get_level(dht_pin) && !ISR_TimerFLAG);
		timer_get_counter_value(TIMER_GROUP_0, TIMER_1, &timerCurrentValue);
		if( timerCurrentValue < uSeg_DataComming-15 || timerCurrentValue > uSeg_DataComming+15 )
			return _DATAERROR;
		timer_set_counter_value(TIMER_GROUP_0, TIMER_1, 0x00000000ULL);
		/* Espero por el dato */
		while( gpio_get_level(dht_pin) && !ISR_TimerFLAG);
		timer_get_counter_value(TIMER_GROUP_0, TIMER_1, &timerCurrentValue);
		if( timerCurrentValue > uSeg_LogicLow+10 )
			/* Recibi un '1' */
			rbuff[nbyte] = rbuff[nbyte] | 1<<pos;
		if(pos == 0){
			pos=7;
			nbyte++;}
		else pos--;
	}
	/* chequeo errores */
	uint8_t checksum = rbuff[0]+rbuff[1]+rbuff[2]+rbuff[3];
	if( checksum != rbuff[4] )
		return _CHECKSUMERROR;
	/* obtengo los valores de la medicion */
	dht_x->temp = rbuff[2];
    if (rbuff[3] & 0x80)
		dht_x->temp = -1 - (dht_x->temp);
    dht_x->temp += (rbuff[3] & 0x0f) * 0.1;
    dht_x->humd = rbuff[0] + rbuff[1] * 0.1;
	return _SUCCESS;
}

void app_dht11(void *arg)
{
	while(1){

		/* Cada 2seg realizo una medicion */
		vTaskDelay(pdMS_TO_TICKS(2000));
		dht11_init_xfer(DHT_SENSOR);//inicio la transferencia
		vTaskSuspendAll();//entro en una zona critica -> suspendo el scheduller
	    uint8_t ret = dht11_read(&dht_var, DHT_SENSOR);
	    xTaskResumeAll();//reanudo el scheduller
    	printf("%s\n", dht_ret_log[ret]);
    	if( ret == _SUCCESS ){
    		/* loggeo los resultados */
    		printf("T = %.2f *C\nH = %.2f  %%\n" , dht_var.temp, dht_var.humd);
    	}
	}
}


