/*
 * timer.c
 *
 *  Created on: 9 mar. 2021
 *      Author: sguti
 */

#include "main.h"

timer_config_t config; //timer struct

/*
 * ------------------------
 * Timer group0 ISR handler
 * ------------------------
 */
void IRAM_ATTR timer_group0_isr(void *ptr_idx)
{
    int timer_idx = (int) ptr_idx;

    /* determino que timer fue el que interrumpio */
    uint32_t timer_intr = timer_group_get_intr_status_in_isr(TIMER_GROUP_0);

    /* Limpio el pedido de interrupcion del timer (t0 o t1) */
    if (timer_intr & TIMER_INTR_T0)
        timer_group_clr_intr_status_in_isr(TIMER_GROUP_0, TIMER_1);
    else if (timer_intr & TIMER_INTR_T1)
    		timer_group_clr_intr_status_in_isr(TIMER_GROUP_0, TIMER_1);

    /* Vuelvo a activar el disparo de interrupcion por alarma del timer */
    timer_group_enable_alarm_in_isr(TIMER_GROUP_0, timer_idx);

    /*levanto el flag de interrupcion por timer*/
    ISR_TimerFLAG = 1;
}

/* ----------------------------------------------
 * Initialize selected timer of the timer group 0
 * ----------------------------------------------
 * -timer_idx: id del timer a inicializar
 * -auto_reload: auto reload enable/disable
 */
void timer_Init(int timer_idx, bool auto_reload)
{
    /* Seteo la estructura del timer */
    config.divider     = TIMER_DIVIDER;
    config.counter_dir = TIMER_COUNT_UP;
    config.counter_en  = TIMER_PAUSE;
    config.alarm_en    = TIMER_ALARM_EN;
    config.intr_type   = TIMER_INTR_LEVEL;
    config.auto_reload = auto_reload;
    timer_init(TIMER_GROUP_0, timer_idx, &config);//transfiero la estructura

    /* Registro la ISR */
    timer_isr_register(TIMER_GROUP_0, timer_idx, timer_group0_isr, (void *) timer_idx, ESP_INTR_FLAG_IRAM, NULL);
    /* Habilito IRQ */
    timer_enable_intr(TIMER_GROUP_0, timer_idx);
}

/* ----------------------------------------------
 * Setea el tiempo de alarma y arranca el timer
 * ----------------------------------------------
 * -timer_idx: id del timer a inicializar
 * -timer_interval_usec: valor al que disparo la alarma
 */
void timer_Count_To(int timer_idx, double timer_interval_usec, bool mode)
{
	/* Pauso el timer */
	timer_pause(TIMER_GROUP_0, timer_idx);
    /* Seteo el contador en 0 */
    timer_set_counter_value(TIMER_GROUP_0, timer_idx, 0x00000000ULL);
    /* Seteo el nivel de disparo de la alarma */
    timer_set_alarm_value(TIMER_GROUP_0, timer_idx, timer_interval_usec);
    /* Timer start ... */
    timer_start(TIMER_GROUP_0, timer_idx);
	ISR_TimerFLAG = 0;//limpio el flag
	if(mode == false){//bloqueante !
		while( !ISR_TimerFLAG );
		ISR_TimerFLAG--;
	}
}

/* ----------------------------------------------
 * Setea el tiempo de alarma y arranca el timer
 * ----------------------------------------------
 * -timer_idx: id del timer a inicializar
 * -timer_interval_usec: valor al que disparo la alarma
 */
void timer_Reset(int timer_idx)
{
	/* Pauso el timer */
	timer_pause(TIMER_GROUP_0, timer_idx);
    /* Seteo el valor inicial del contador */
    timer_set_counter_value(TIMER_GROUP_0, timer_idx, 0x00000000ULL);
    /* Timer start ... */
    timer_start(TIMER_GROUP_0, timer_idx);
}

