/*
*/
#include "main.h"

#include "lwip/err.h"
#include "lwip/sys.h"
#include "lwip/apps/sntp.h"
#include "esp_sleep.h"

#define ESP_WIFI_SSID	  "Chibuchacha"
#define ESP_WIFI_PASS 	  "fijateenlaheladera"
#define ESP_MAXIMUM_RETRY  10

#define SPIN_TASK_PRIO 2
extern const char *TAG;
#define BLINK_LED 22 //pin led

#define NO_RETRY_SYNC 10
char strftime_buf[64];
QueueHandle_t time_sem_handler;

/*FreeRTOS event group to signal when we are connected*/
EventGroupHandle_t s_wifi_event_group;
/* The event group allows multiple bits for each event, but we only care about two events:
 * - we are connected to the AP with an IP
 * - we failed to connect after the maximum amount of retries */
#define WIFI_CONNECTED_BIT BIT0
#define WIFI_FAIL_BIT      BIT1
int s_retry_num = 0;

void event_handler(void* arg, esp_event_base_t event_base, int32_t event_id, void* event_data)
{
    if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START) {
        esp_wifi_connect();}
    else  if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED) {
          	  if (s_retry_num < ESP_MAXIMUM_RETRY) {
          		  esp_wifi_connect();
        		  s_retry_num++;
        		  ESP_LOGI(TAG, "%d: retry to connect to the AP", s_retry_num);
        	  }
        	  else {
        		xEventGroupSetBits(s_wifi_event_group, WIFI_FAIL_BIT);}
           ESP_LOGI(TAG,"connect to the AP fail");
    	   }
    else   if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP) {
    	   	   ip_event_got_ip_t* event = (ip_event_got_ip_t*) event_data;
    		   ESP_LOGI(TAG, "got ip:" IPSTR, IP2STR(&event->ip_info.ip));
    		   s_retry_num = 0;
    		   xEventGroupSetBits(s_wifi_event_group, WIFI_CONNECTED_BIT);
    	   }
}

void wifi_init_sta(void)
{
    s_wifi_event_group = xEventGroupCreate();

    ESP_ERROR_CHECK(esp_netif_init());

    ESP_ERROR_CHECK(esp_event_loop_create_default());
    esp_netif_create_default_wifi_sta();

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    esp_event_handler_instance_t instance_any_id;
    esp_event_handler_instance_t instance_got_ip;
    ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT,
                                                        ESP_EVENT_ANY_ID,
                                                        &event_handler,
                                                        NULL,
                                                        &instance_any_id));
    ESP_ERROR_CHECK(esp_event_handler_instance_register(IP_EVENT,
                                                        IP_EVENT_STA_GOT_IP,
                                                        &event_handler,
                                                        NULL,
                                                        &instance_got_ip));
    wifi_config_t wifi_config = {
        .sta = {
            .ssid = ESP_WIFI_SSID,
            .password = ESP_WIFI_PASS,
            /* Setting a password implies station will connect to all security modes including WEP/WPA.
             * However these modes are deprecated and not advisable to be used. Incase your Access point
             * doesn't support WPA2, these mode can be enabled by commenting below line */
	     .threshold.authmode = WIFI_AUTH_WPA2_PSK,

            .pmf_cfg = {
                .capable = true,
                .required = false
            },
        },
    };
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA) );
    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config) );
    ESP_ERROR_CHECK(esp_wifi_start() );

    ESP_LOGI(TAG, "wifi_init_sta finished.");

    /* Waiting until either the connection is established (WIFI_CONNECTED_BIT) or connection failed for the maximum
     * number of re-tries (WIFI_FAIL_BIT). The bits are set by event_handler() (see above) */
    EventBits_t bits = xEventGroupWaitBits(s_wifi_event_group,
            WIFI_CONNECTED_BIT | WIFI_FAIL_BIT,
            pdFALSE,
            pdFALSE,
            portMAX_DELAY);

    /* xEventGroupWaitBits() returns the bits before the call returned, hence we can test which event actually
     * happened. */
    if (bits & WIFI_CONNECTED_BIT) {
        ESP_LOGI(TAG, "connected to ap SSID:%s password:%s",
        		ESP_WIFI_SSID, ESP_WIFI_PASS);
    } else if (bits & WIFI_FAIL_BIT) {
        ESP_LOGI(TAG, "Failed to connect to SSID:%s, password:%s",
        		ESP_WIFI_SSID, ESP_WIFI_PASS);
    } else {
        ESP_LOGE(TAG, "UNEXPECTED EVENT");
    }

    /* The event will not be processed after unregister */
    ESP_ERROR_CHECK(esp_event_handler_instance_unregister(IP_EVENT, IP_EVENT_STA_GOT_IP, instance_got_ip));
    ESP_ERROR_CHECK(esp_event_handler_instance_unregister(WIFI_EVENT, ESP_EVENT_ANY_ID, instance_any_id));
    vEventGroupDelete(s_wifi_event_group);
}

void time_sync_notification_cb(struct timeval *tv)
{
    ESP_LOGI(TAG, "Evento --- Sincronizado correctamente");
}

void SNTPinit(void)
{
	ESP_LOGI(TAG, "Inicializando SNTP");
	sntp_setoperatingmode(SNTP_OPMODE_POLL);
	sntp_setservername(0, "pool.ntp.org");
    sntp_set_time_sync_notification_cb(time_sync_notification_cb);/* callback que se ejecuta en cada sincronizacion */
	sntp_set_sync_mode(SNTP_SYNC_MODE_IMMED);
	sntp_init();
}

int obtain_time(void)
{
    time_t now = 0;
    struct tm timeinfo = { 0 };
    int retry_count = 0;
    while(sntp_get_sync_status() == SNTP_SYNC_STATUS_RESET && (++retry_count < NO_RETRY_SYNC) ) {
        ESP_LOGI(TAG, "System time: sincronizando ... (%d/10)", retry_count );
        vTaskDelay(2000 / portTICK_PERIOD_MS);
    }
    if( retry_count == NO_RETRY_SYNC )
    	return -1;
    time(&now);
    localtime_r(&now, &timeinfo);
    return 0;
}

void app_time( void * arg )
{
	time_t now;
	struct tm timeinfo;

	/* Inicializo NTP */
	SNTPinit();
	/* Sincronizo con el servidor */
    int ret = obtain_time();
    if(ret == -1){
        const int deep_sleep_sec = 10;
        ESP_LOGI(TAG, "Entering deep sleep for %d seconds", deep_sleep_sec);
        esp_deep_sleep(1000000LL * deep_sleep_sec);
    }
    else {
    	time(&now);// Actualizo con el tiempo actual
    	/* Seteo timezone: Buenos aires (UTC+3) */
    	setenv("TZ", "UTC+3", 1);
    	tzset();
    	localtime_r(&now, &timeinfo);
    	strftime(strftime_buf, sizeof(strftime_buf), "%c", &timeinfo);
    	ESP_LOGI(TAG, "Buenos Aires UTC+3 --- %s", strftime_buf);

    	while(1) {
    		/*Bloquea hasta que se libere el semaforo*/
    		xSemaphoreTake(time_sem_handler, portMAX_DELAY);
    		time(&now);
    		localtime_r(&now, &timeinfo);
    		strftime(strftime_buf, sizeof(strftime_buf), "%c", &timeinfo);
        	ESP_LOGI(TAG, "Buenos Aires UTC+3 --- %s", strftime_buf);
    	}
    }
}

void app_blinking( void * arg )
{
    /* Configuro el registro IOMUX para el pin BLINK_LED */
    gpio_pad_select_gpio(BLINK_LED);
    /* Seteo el pin como salida */
    gpio_set_direction(BLINK_LED, GPIO_MODE_OUTPUT);

	bool toggle = true;

	while(1){
		gpio_set_level(BLINK_LED, toggle);
		toggle ^= true;
		vTaskDelay(pdMS_TO_TICKS(500));
	}
}

void app_main(void)
{
    /* Inicializo NVS */
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
      ESP_ERROR_CHECK(nvs_flash_erase());
      ret = nvs_flash_init();}
    ESP_ERROR_CHECK(ret);
    ESP_LOGI(TAG, "ESP_WIFI_MODE_STA");
    /* Inicializo WiFi */
    wifi_init_sta();
    /* Inicializo spiffs */
    if( spiffs_init("/spiffs") == -1 )
        ESP_LOGE(TAG, "Error al inicializar /spiffs");
    /* Inicializo el timer 1 */
    timer_Init(TIMER_1, AUTO_RELOAD);
    /* Inicializo el DHT11 */
    dht11_init(DHT_SENSOR);
    /* Inicializo el semaforo para sincronizar system time */
    time_sem_handler = xSemaphoreCreateBinary();

    /* Creo las tareas */
    xTaskCreatePinnedToCore(app_blinking    , "blinking task"  , 2048, NULL, SPIN_TASK_PRIO, NULL, tskNO_AFFINITY);
    xTaskCreatePinnedToCore(app_http_server , "HTTPServer task", 4096, NULL, SPIN_TASK_PRIO, NULL, tskNO_AFFINITY);
    xTaskCreatePinnedToCore(app_dht11       , "dht11 task"     , 4096, NULL, SPIN_TASK_PRIO, NULL, tskNO_AFFINITY);
	xTaskCreatePinnedToCore(app_time        ,  "time task"     , 2048, NULL, SPIN_TASK_PRIO, NULL, tskNO_AFFINITY);
}
